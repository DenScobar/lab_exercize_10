'use strict';

const personalMovieDB = {
    count: 0,
    movies: {},
    actors: {},
    genres: [],
    privat: false,

    start: function () {
        let numberOfFilms;
        do {
            numberOfFilms = prompt('Сколько фильмов вы уже посмотрели?');
        }
        while (numberOfFilms == "" || numberOfFilms == null || numberOfFilms.length > 50 ||
        isNaN(+numberOfFilms) || !(Number.isSafeInteger(+numberOfFilms)) || numberOfFilms < 0)
        personalMovieDB.count = numberOfFilms;
    },

    rememberMyFilms: function () {

        let movieTitle;
        let filmRating;

        do {
            movieTitle = prompt('Один из последних просмотренных фильмов?');
        }
        while (movieTitle == "" || movieTitle == null || movieTitle.length > 50 || !(isNaN(+movieTitle)))
        do {
            filmRating = prompt('На сколько оцените его? (от 0 до 10)');
        }
        while (filmRating == "" || filmRating == null || filmRating > 10 || filmRating < 0 || isNaN(+filmRating))

        personalMovieDB.movies[movieTitle] = filmRating;
    },

    detectPersonalLevel: function () {
        if (+personalMovieDB.count < 10) {
            console.log("Просмотрено довольно мало фильмов");
        } else if (+personalMovieDB.count < 30 && +personalMovieDB.count >= 10) {
            console.log("Вы классический зритель");
        } else if (+personalMovieDB.count >= 30) {
            console.log("Вы киноман");
        } else {
            console.log("Произошла ошибка");
        }
    },

    showMyDB: function () {
        if (!(personalMovieDB.privat)) {
            console.log(this);
        }
    },

    writeYourGenres: function () {
        const genreTitle = [];
        for (let i = 0; i < 3; i++) {
            do {
                genreTitle[i] = prompt(`Ваш любимый жанр под номером ${i + 1}`);
            }
            while (genreTitle[i] == "" || genreTitle[i] == null || genreTitle[i].length > 50 || !(isNaN(+genreTitle[i])))
        }
        personalMovieDB.genres = genreTitle;

        personalMovieDB.genres.forEach((element, index) => {
            console.log(`Любимый жанр №${index + 1} - это ${element}`);
        });
    },

    toggleVisibleMyDB: function () {
        if (!(personalMovieDB.privat)) {
            personalMovieDB.privat = true;
        } else {
            personalMovieDB.privat = false;
        }
    },
}
personalMovieDB.start();
personalMovieDB.rememberMyFilms();
personalMovieDB.detectPersonalLevel();
personalMovieDB.writeYourGenres();
personalMovieDB.showMyDB();
personalMovieDB.toggleVisibleMyDB();
personalMovieDB.showMyDB();
personalMovieDB.showMyDB();
// personalMovieDB.toggleVisibleMyDB();







